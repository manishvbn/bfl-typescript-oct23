// // ------------------------------------ Rest parameter
// // Rest Parameter should always be the last parameter
// // Rest Parameter is an array of values

// function Average(...args: number[]): number {
//     if (args.length > 0) {
//         let sum = args.reduce((x, y) => x + y);
//         return sum / args.length;
//     } else {
//         return 0;
//     }
// }

// console.log(Average());
// console.log(Average(1));
// console.log(Average(1, 2));
// console.log(Average(1, 2, 3, 4, 5));
// console.log(Average(1, 2, 3, 4, 5, 6, 7, 8, 9));

// // Combine comma seperated items into a Array (...) - Rest Parameter
// // ... used in function parameter at the time of function creation - Rest Parameter
// // ... on Left hand side of assignment operator - Rest Parameter

// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// // console.log(Average(numbers));
// console.log(Average(...numbers));       // Spread Operator - Array Spread

// // Split Array/Object to a comma seperated items - Spread Operator
// // ... used in function argument at the time of function call - Spread Operator
// // ... on Right hand side of assignment operator - Spread Operator


// // ------------------ Query
// // function genUrl(id: number, ...args: number[]) {
// //     console.log(id);
// //     console.log(args);
// // }

// // genUrl(1, ...[2, 3, 4, 5, 6, 7, 8, 9]);

// // Create a Fn which accepts an object

// // function fn(arg: { id: number, ename: string, city: string, state: string, pin: number }) {

// // }


// // function fn(args: { id: number, ename: string, ...address: { city: string, state: string, pin: number } }) {

// // }

// // fn({ id: 1, ename: "Manish", city: "Pune", state: "MH", pin: 411021 });

// var emp = { id: 1, ename: "Manish", city: "Pune", state: "MH", pin: 411021 };
// var { id, ename, ...address } = emp;            // Destructuring with Rest Operator

// console.log("Id: ", id);
// console.log("Name: ", ename);
// console.log("Address: ", address);

// <HelloComponent id={ 1 } ename = { "Manish"} city={ "Pune"}, state={ "MH"} />

//     function HelloComponent(props: { id: number, ename: string}) {

//     }

// type TEmployee = {
//     id: number,
//     ename: string,
//     city: string,
//     state: string,
//     pin: number
// }

// interface TEmployee {
//     id: number;
//     ename: string;
//     city: string;
//     state: string;
//     pin: number;
// }

// function Hello(args: TEmployee) {
//     const { id, ename, ...address } = args;

//     console.log(`Employee ID: ${id}`);
//     console.log(`Employee Name: ${ename}`);
//     console.log(`Address: `, address);
// }

// function Hello({ id, ename, ...address }: TEmployee) {
//     console.log(`Employee ID: ${id}`);
//     console.log(`Employee Name: ${ename}`);
//     console.log(`Address: `, address);
// }

// Hello({ id: 1, ename: "Manish", city: "Pune", state: "MH", pin: 411021 });
