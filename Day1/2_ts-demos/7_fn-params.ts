// function hello_ts(name: string) {
//     console.log("Hello " + name);
// }

// hello_ts("John");
// hello_ts(10);
// hello_ts("Manish", "Sharma");
// hello_ts();

// // ------------------------------------------------- Optional Parameters (?)
// // A required parameter cannot follow an optional parameter.
// function Add(a?: number, b?: number): number {
//     if (a === void 0) a = 0;
//     if (b === void 0) b = 0;

//     return a + b;
// }

// console.log(Add(2, 3));
// console.log(Add(2));
// console.log(Add());

// ------------------------------------------------- Default Parameters 
// A required parameter cannot follow an default parameter.
function Add(a = 0, b = 0): number {
    return a + b;
}

console.log(Add(2, 3));
console.log(Add(2));
console.log(Add());