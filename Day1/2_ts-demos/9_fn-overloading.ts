// function hello() {
//     console.log("Hello World");
// }

// Duplicate function implementation.
// function hello(name: string) {
//     console.log("Hello " + name);
// }

// hello();
// hello("Manish");

// ---------------------------------------------------------
function hello(): void;
function hello(name: string): void;

function hello(...args: string[]) {
    if (args.length === 0) {
        console.log("Hello World");
    } else {
        console.log("Hello " + args[0]);
    }
}

hello();
hello("Manish");
// hello("Manish", "Pune");