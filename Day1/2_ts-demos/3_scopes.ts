// Global Scope (If Module is disabled in tsconfig.json)
// NameSpace Scope
// Module Scope (If Module is enabled in tsconfig.json)
// Function(Local) Scope
// Block Scope - Only if we use let or const

// var i = 20;
// console.log(i);

// namespace Demo {
//     var i = 20;
//     console.log(i);
// }

namespace Demo {
    export var i = 20;
}

namespace Usage {
    console.log(Demo.i);
}