function hello_js(name) {
    console.log("Hello " + name);
}

hello_js("John");
hello_js(10);
hello_js("Manish", "Sharma");
hello_js();