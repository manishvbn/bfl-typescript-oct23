// Variables created in TypeScript are optionally typesafe
// Untyped Variable - Not TypeSafe, we will not get any intellisense on an untyped variable (any)
// var data;
// data = 10;
// data = "Hello";

// var data: any;
// data = 10;
// data = "Manish";

// Implicitly Typed Variable - TypeSafe, We will get intellisense on implicitly typed variable
// var data = 10;
// // data = "Hello"; // Error    

// var ename = "Manish";
// // ename = 10; // Error

// // Explicitly Typed Variable - TypeSafe, We will get intellisense on explicitly typed variable
// var age: number;
// age = 20;
// // age = "Manish"; // Error


// Function to add 2 number
// function add(x, y) {
//     return x + y;
// }

// function add(x: any, y: any) {
//     return x + y;
// }

function add(x: number, y: number) {
    return x + y;
}

console.log(add(2, 3));
// console.log(add(2, "abc"));
// console.log(add("abc", "xyz"));

// number / string / boolean / undefined / array / object / Date / RegExpo / Function / void
// All the new types which are supported by the latest version of ECMASCRIPT
// Lefthand side of assignment operator, all JS Types can be used (Declaration)

var a: Array<string>;
var s: Symbol;
var p: Promise<string>;

// Righthand side of assignment operator, API's (Functions or Types) will come
// If you want to use any API, You can only use them with proper configuration
// Based on target in tsconfig.json
// And lib section configured in your tsconfig.json

a = new Array<string>();
s = Symbol("Manish");
p = new Promise<string>((resolve, reject) => {
    resolve("Manish");
});

navigator