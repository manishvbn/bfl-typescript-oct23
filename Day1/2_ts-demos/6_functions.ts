// function hello() {
//     console.log("Hello World!");
// }

// // var r = hello();
// // console.log(r);
// // console.log(typeof r);

// // var r1: undefined;
// // // r1 = 10;        // Type '10' is not assignable to type 'undefined'.
// // r1 = undefined;
// // r1 = void 0;
// // console.log(r1);

// // var r2: void;
// // // r2 = 10;        // Type 'number' is not assignable to type 'void'.
// // r2 = undefined;
// // r2 = void 0;
// // console.log(r2);

// // var r3: never;
// // r3 = 10;
// // r3 = undefined;
// // r3 = void 0;    
// // console.log(r3);

// // var r4: any;
// // console.log(r4.toFixed());

// // var r5: unknown;
// // console.log(r5.toFixed());

// // -------------------------------------

// // function iterate() {
// //     let i = 1;
// //     while (true) {
// //         console.log(++i);
// //     }
// // }

// // function iterate(): never {
// //     let i = 1;
// //     while (true) {
// //         console.log(++i);
// //     }
// // }

// // iterate();
// // console.log("Last Line in the file...");

// // Write a function to log a message passed and throw exception to terminate the execution


// var app: any;

// app.get('/index', (req: any, res: any) => {
//     try {
//         // Database Call
//     } catch (e) {
//         logAndTerminate(e, req, res);
//         console.log("Some code to run after I send the response");
//     }
// });

// function logAndTerminate(err: any, req: any, res: any): never {
//     console.log(err);       // Or log using logger sevice of your choice
//     res.send("Something went wrong!");
//     throw new Error();
// }

// -------------------------------------------

// Function Declaration
function add1(a: number, b: number): number {
    return a + b;
}

// Function Expression
const add2 = function (a: number, b: number): number {
    return a + b;
}

let add3: (x: number, y: number) => number;
add3 = function (a: number, b: number): number {
    return a + b;
}

let add4: (x: number, y: number) => number;
add4 = function (a, b) {
    return a + b;
}

// Multiline Lambda
let add5: (x: number, y: number) => number;
add5 = (a, b) => {
    return a + b;
}

// Singleline Lambda
let add6: (x: number, y: number) => number;
add6 = (a, b) => a + b;

console.log(add1(2, 3));
console.log(add2(2, 3));
console.log(add3(2, 3));
console.log(add4(2, 3));
console.log(add5(2, 3));
console.log(add6(2, 3));