console.log("Hello from Demo Three");
var Person = /** @class */ (function () {
    function Person(name) {
        var _this = this;
        this.name = name;
        this.getName = function () {
            return _this.name;
        };
        this.setName = function (name) {
            _this.name = name;
        };
    }
    return Person;
}());
var p1 = new Person("Manish");
console.log(p1.getName());
p1.setName("Abhijeet");
console.log(p1.getName());
var p2 = new Person("Subodh");
console.log(p2.getName());
p2.setName("Ramakant");
console.log(p2.getName());
console.log(p1);
console.log(p2);
