// const area = function (rect: { h: number, w?: number }) {
//     rect.w = rect.w || rect.h;
//     return rect.h * rect.w;
// }

// let s1 = { h: 20, w: 10 };
// console.log(area(s1));

// let s2 = { h: 20, w: 10, d: 30 };
// console.log(area(s2));

// let s3 = { h: 20, d: 30 };
// console.log(area(s3));

// // ------------------------------------------- Type Alias

// type Shape = { h: number, w?: number };

// const area = function (rect: Shape) {
//     rect.w = rect.w || rect.h;
//     return rect.h * rect.w;
// }

// let s1: Shape = { h: 20, w: 10 };
// console.log(area(s1));

// // let s2: Shape = { h: 20, w: 10, d: 30 };
// // console.log(area(s2));

// // let s3: Shape = { h: 20, d: 30 };
// // console.log(area(s3));

// // ------------------------------------------- Interface

// interface Shape {
//     h: number;
//     w?: number;
// }

// const area = function (rect: Shape) {
//     rect.w = rect.w || rect.h;
//     return rect.h * rect.w;
// }

// let s1: Shape = { h: 20, w: 10 };
// console.log(area(s1));

// let s2: Shape = { h: 20, w: 10, d: 30 };
// console.log(area(s2));

// let s3: Shape = { h: 20, d: 30 };
// console.log(area(s3));

// ----------------------------------------------------

// interface IPerson {
//     name: string;
//     age: number;
//     greet(message: unknown): unknown;
// }

// let p1: IPerson = {
//     name: 'Magesh',
//     age: 40,
//     greet: function (message: string): string {
//         return "Hi";
//     }
// };

// let p2: IPerson = {
//     name: 'Nikhil',
//     age: 30,
//     greet: function (message: string): string {
//         return "Hola";
//     }
// };

// console.log(p1.greet("Hi"));
// console.log(p2.greet("Hi"));

// type TPerson = {
//     name: string,
//     age: number,
//     greet(message: unknown): unknown
// }

// let p3: TPerson = {
//     name: 'Manish',
//     age: 30,
//     greet: function (message: string): string {
//         return "Hey";
//     }
// };

// console.log(p3.greet("Hi"));

// ----------------------------------------------------
// Duplicate identifier 'TShape'
// type TShape = {
//     height: number
// }

// type TShape = {
//     width: number
// }

// // Interface Merging
// interface IShape {
//     height: number;
// }

// interface IShape {
//     width: number;
// }

// let s1: IShape = {
//     height: 10,
//     width: 20
// };

// ----------------------------------------------------- Type Alias and Interface Used Together

// interface ICustomer {
//     doShopping(): string;
// }

// interface IEmployee extends ICustomer {
//     doWork(): string;
// }

// type ICustomerOrEmployee = ICustomer | IEmployee;

// var person1: ICustomerOrEmployee = {
//     doShopping: function () {
//         return "Shopping Online";
//     }
// }

// // Intersection
// type ICustomerAndEmployee = ICustomer & IEmployee;

// var person2: ICustomerAndEmployee = {
//     doShopping: function () {
//         return "Shopping Online";
//     },
//     doWork: function () {
//         return "Working";
//     }
// }